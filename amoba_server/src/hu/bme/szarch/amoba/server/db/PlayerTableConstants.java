package hu.bme.szarch.amoba.server.db;

public class PlayerTableConstants {
	public static final String COL_USER_NAME = "user_name";
	public static final String COL_USER_PW = "user_pw";
	public static final String COL_WINS = "win_count";
	public static final String COL_LOSSES = "loss_count";
	public static final String COL_ONLINE = "is_online";
	public static final String COL_CURRENT_GAME_ID = "current_game_id";

	public static final String TABLE_NAME = "players";

	//@formatter:off
	public static final String CREATE_TABLE_STATEMENT = "CREATE TABLE IF NOT EXISTS "+ TABLE_NAME 
			+ "(" 
			+ COL_USER_NAME + " TEXT PRIMARY KEY NOT NULL, "
			+ COL_USER_PW + " TEXT NOT NULL, " 
			+ COL_WINS + " INTEGER DEFAULT 0, "
			+ COL_LOSSES + " INTEGER DEFAULT 0, "
			+ COL_ONLINE + " INTEGER DEFAULT 0, "
			+ COL_CURRENT_GAME_ID + " INTEGER DEFAULT -1)";
	//@formatter:on
}
