package hu.bme.szarch.amoba.server.db;

public class GameTableConstants {
	public static final String COL_GAME_ID = "game_id";
	public static final String COL_PLAYER_O_NAME = "player_o_name";
	public static final String COL_PLAYER_X_NAME = "player_x_name";
	public static final String COL_WIDTH = "width";
	public static final String COL_HEIGHT = "height";
	public static final String COL_WIN_CRITERIA = "win_crit";
	public static final String COL_FILE_DIR = "file_dir";

	public static final String TABLE_NAME = "games";

	//@formatter:off
	public static final String CREATE_TABLE_STATEMENT = "CREATE TABLE IF NOT EXISTS "+ TABLE_NAME 
			+ "(" 
			+ COL_GAME_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ COL_PLAYER_O_NAME + " TEXT, " 
			+ COL_PLAYER_X_NAME + " TEXT, "
			+ COL_WIDTH + " INTEGER NOT NULL, "
			+ COL_HEIGHT + " INTEGER NOT NULL, "
			+ COL_WIN_CRITERIA + " INTEGER NOT NULL, "
			+ COL_FILE_DIR+ " TEXT)";
	//@formatter:on
}
