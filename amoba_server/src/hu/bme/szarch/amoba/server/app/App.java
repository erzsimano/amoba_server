package hu.bme.szarch.amoba.server.app;

import hu.bme.szarch.amoba.common.config.Config;
import hu.bme.szarch.amoba.common.network.NetworkMessageRouter;
import hu.bme.szarch.amoba.common.network.MessageReceiverThread;
import hu.bme.szarch.amoba.server.service.managers.DefaultGameManager;
import hu.bme.szarch.amoba.server.service.managers.DefaultPlayerManager;
import hu.bme.szarch.amoba.server.service.managers.SQLiteDatabaseManager;
import hu.bme.szarch.amoba.server.service.managers.base.DatabaseManager;
import hu.bme.szarch.amoba.server.service.managers.base.GameManager;
import hu.bme.szarch.amoba.server.service.managers.base.PlayerManager;
import hu.bme.szarch.amoba.server.service.messagehandlers.CreateGameMessageHandler;
import hu.bme.szarch.amoba.server.service.messagehandlers.JoinGameMessageHandler;
import hu.bme.szarch.amoba.server.service.messagehandlers.LoginMessageHandler;
import hu.bme.szarch.amoba.server.service.messagehandlers.LogoutMessageHandler;
import hu.bme.szarch.amoba.server.service.messagehandlers.MarkMessageHandler;
import hu.bme.szarch.amoba.server.service.messagehandlers.ServerMessageRouter;

public class App {

	private static DatabaseManager dbManager = null;

	private static PlayerManager playerManager = null;

	private static GameManager gameManager = null;

	private static NetworkMessageRouter messageRouter = null;

	public static void main(String[] args) {
		setUpManagers();
		setUpMessageRouting();
		startListeningForIncomingConnections();
	}

	private static void setUpManagers() {
		dbManager = SQLiteDatabaseManager.getInstance();

		playerManager = new DefaultPlayerManager(dbManager);

		gameManager = new DefaultGameManager(dbManager, playerManager);
	}

	private static void setUpMessageRouting() {
		messageRouter = new ServerMessageRouter();

		messageRouter.registerMessageHandler(new LoginMessageHandler(playerManager));
		messageRouter.registerMessageHandler(new LogoutMessageHandler(playerManager));
		messageRouter.registerMessageHandler(new CreateGameMessageHandler(gameManager));
		messageRouter.registerMessageHandler(new JoinGameMessageHandler(gameManager));
		messageRouter.registerMessageHandler(new MarkMessageHandler(gameManager));
	}

	private static void startListeningForIncomingConnections() {

		MessageReceiverThread receiverThread = new MessageReceiverThread(Config.SERVER_PORT);
		receiverThread.addMessageRouter(messageRouter);

		receiverThread.start();
	}

}
