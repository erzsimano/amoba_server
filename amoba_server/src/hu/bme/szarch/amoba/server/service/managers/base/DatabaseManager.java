package hu.bme.szarch.amoba.server.service.managers.base;

import java.sql.SQLException;
import java.util.List;

import hu.bme.szarch.amoba.common.game.GameData;
import hu.bme.szarch.amoba.common.player.Player;
import hu.bme.szarch.amoba.server.exception.GameNotFoundException;

public interface DatabaseManager {

	public void deleteGame(int gameId) throws SQLException;

	public GameData getGame(int gameId, GameManager gameManager) throws SQLException;

	public List<GameData> listOpenGames(GameManager gameManager) throws SQLException;

	public Player getPlayer(String userName) throws SQLException;

	public int createGame(String creatorUserName, int width, int height, int winCriteria) throws SQLException;

	public void registerPlayer(String userName, String password) throws SQLException;

	public void login(String userName) throws SQLException;

	public void logout(String userName) throws SQLException;

	public void deletePlayer(String userName) throws SQLException;

	public void addPlayerToGame(int gameId, String joinUserName, GameManager gameManager)
			throws SQLException, GameNotFoundException;

	public void addPlayerWin(String userName) throws SQLException;

	public void addPlayerLoss(String userName) throws SQLException;

	public void open() throws SQLException, ClassNotFoundException;

	public void createTables() throws SQLException;

	public void close() throws SQLException;
}
