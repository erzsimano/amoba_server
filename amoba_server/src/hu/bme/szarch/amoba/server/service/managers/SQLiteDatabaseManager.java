package hu.bme.szarch.amoba.server.service.managers;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import hu.bme.szarch.amoba.common.game.GameData;
import hu.bme.szarch.amoba.common.player.Player;
import hu.bme.szarch.amoba.server.db.GameTableConstants;
import hu.bme.szarch.amoba.server.db.PlayerTableConstants;
import hu.bme.szarch.amoba.server.exception.GameNotFoundException;
import hu.bme.szarch.amoba.server.service.managers.base.DatabaseManager;
import hu.bme.szarch.amoba.server.service.managers.base.GameManager;

public class SQLiteDatabaseManager implements DatabaseManager {

	private static SQLiteDatabaseManager instance = null;
	private static final String DB_NAME = "amoba_server_db.db";

	private static Connection dbConnection;

	private static int referenceCount = 0;

	private SQLiteDatabaseManager() {

	}

	@Override
	public void createTables() throws SQLException {

		// Create player table
		executeUpdateStatement(PlayerTableConstants.CREATE_TABLE_STATEMENT);

		// Create game table
		executeUpdateStatement(GameTableConstants.CREATE_TABLE_STATEMENT);
	}

	private void executeUpdateStatement(String sql) throws SQLException {
		Statement updateStatement = dbConnection.createStatement();
		updateStatement.executeUpdate(sql);
		updateStatement.close();
	}

	public static DatabaseManager getInstance() {
		if (instance == null)
			instance = new SQLiteDatabaseManager();

		return instance;
	}

	@Override
	public void open() throws SQLException, ClassNotFoundException {
		Class.forName("org.sqlite.JDBC");
		dbConnection = DriverManager.getConnection("jdbc:sqlite:" + DB_NAME);
		System.out.println("Opened database successfully");

		incrementReferenceCount();
	}

	@Override
	public void close() throws SQLException {
		if (referenceCount > 0) {
			decrementReferenceCount();
		} else if (!dbConnection.isClosed()) {
			dbConnection.close();
		}
	}

	private void decrementReferenceCount() {
		if (referenceCount != 0)
			referenceCount--;
	}

	private void incrementReferenceCount() {
		referenceCount++;
	}

	@Override
	public void deleteGame(int gameId) throws SQLException {
		String sql = "DELETE FROM " + GameTableConstants.TABLE_NAME + " WHERE " + GameTableConstants.COL_GAME_ID + "="
				+ gameId + ";";

		executeUpdateStatement(sql);
	}

	@Override
	public GameData getGame(int gameId, GameManager gameManager) throws SQLException {
		GameData game = null;

		String sql = "SELECT * FROM " + GameTableConstants.TABLE_NAME + " WHERE " + GameTableConstants.COL_GAME_ID + "="
				+ gameId + ";";
		Statement statement = dbConnection.createStatement();
		ResultSet rs = statement.executeQuery(sql);

		while (rs.next())
			game = gameManager.createGame(rs);

		rs.close();
		statement.close();
		return game;
	}

	@Override
	public List<GameData> listOpenGames(GameManager gameManager) throws SQLException {
		List<GameData> games = new ArrayList<>();
		String sql = "SELECT * FROM " + GameTableConstants.TABLE_NAME + " WHERE " + GameTableConstants.COL_PLAYER_O_NAME
				+ " IS NULL OR " + GameTableConstants.COL_PLAYER_X_NAME + " IS NULL;";
		Statement statement = dbConnection.createStatement();
		ResultSet rs = statement.executeQuery(sql);

		while (rs.next()) {
			GameData game = gameManager.createGame(rs);
			games.add(game);
		}
		return games;
	}

	@Override
	public Player getPlayer(String userName) throws SQLException {
		Player player = null;

		String sql = "SELECT * FROM " + PlayerTableConstants.TABLE_NAME + " WHERE " + PlayerTableConstants.COL_USER_NAME
				+ "=" + userName + ";";
		Statement statement = dbConnection.createStatement();
		ResultSet rs = statement.executeQuery(sql);

		while (rs.next())
			player = new Player(rs.getString(PlayerTableConstants.COL_USER_NAME), null);

		rs.close();
		statement.close();
		return player;
	}

	@Override
	public int createGame(String creatorUserName, int width, int height, int winCriteria) throws SQLException {
		int gameId = -1;

		String sql = "INSERT INTO " + GameTableConstants.TABLE_NAME + "(" + GameTableConstants.COL_PLAYER_O_NAME + ","
				+ GameTableConstants.COL_WIDTH + "," + GameTableConstants.COL_HEIGHT + ","
				+ GameTableConstants.COL_WIN_CRITERIA + ") VALUES ('" + creatorUserName + "', " + width + ", " + height
				+ ", " + winCriteria + ");";
		Statement statement = dbConnection.createStatement();
		executeUpdateStatement(sql);

		sql = "SELECT max(" + GameTableConstants.COL_GAME_ID + ") AS id FROM " + GameTableConstants.TABLE_NAME + ";";
		statement = dbConnection.createStatement();
		ResultSet rs = statement.executeQuery(sql);

		while (rs.next())
			gameId = rs.getInt("id");

		return gameId;
	}

	@Override
	public void addPlayerToGame(int gameId, String joinUserName, GameManager gameManager)
			throws SQLException, GameNotFoundException {
		GameData game = getGame(gameId, gameManager);

		if (game != null) {
			String sql = "UPDATE " + GameTableConstants.TABLE_NAME + " SET " + GameTableConstants.COL_PLAYER_X_NAME
					+ "='" + joinUserName + "';";
			executeUpdateStatement(sql);

		} else
			throw new GameNotFoundException();
	}

	@Override
	public void registerPlayer(String userName, String password) throws SQLException {

	}

	@Override
	public void deletePlayer(String userName) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void addPlayerWin(String userName) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void addPlayerLoss(String userName) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void login(String userName) throws SQLException {
		// TODO Auto-generated method stub

	}

	@Override
	public void logout(String userName) throws SQLException {
		// TODO Auto-generated method stub

	}

}
