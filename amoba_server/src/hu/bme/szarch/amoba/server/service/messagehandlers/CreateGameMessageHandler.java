package hu.bme.szarch.amoba.server.service.messagehandlers;

import static hu.bme.szarch.amoba.common.config.Config.ID_CREATE_GAME;

import hu.bme.szarch.amoba.common.config.Config.HostGameFields;
import hu.bme.szarch.amoba.common.network.NetworkMessage;
import hu.bme.szarch.amoba.server.service.managers.base.GameManager;
import hu.bme.szarch.amoba.server.service.messagehandlers.base.GameMessageHandler;

public class CreateGameMessageHandler extends GameMessageHandler {

	public CreateGameMessageHandler(GameManager gameManager) {
		super(gameManager);
	}

	@Override
	public boolean handleMessage(NetworkMessage message) {
		if (!message.getId().equals(getHandleId()))
			return false;

		String creatorUserName = message.getData(HostGameFields.DATA_CREATOR_USERNAME);
		int width = Integer.valueOf(message.getData(HostGameFields.DATA_CREATE_GAME_WIDTH));
		int height = Integer.valueOf(message.getData(HostGameFields.DATA_CREATE_GAME_HEIGHT));
		int winCriteria = Integer.valueOf(message.getData(HostGameFields.DATA_CREATE_GAME_WIN_CRITERIA));

		gameManager.createGame(creatorUserName, width, height, winCriteria);

		return true;
	}

	@Override
	public String getHandleId() {
		return ID_CREATE_GAME;
	}

}
