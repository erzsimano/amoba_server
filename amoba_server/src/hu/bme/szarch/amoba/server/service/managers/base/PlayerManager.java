package hu.bme.szarch.amoba.server.service.managers.base;

import java.sql.ResultSet;

import hu.bme.szarch.amoba.common.player.Player;

public interface PlayerManager {
	public boolean logoutUser(String userName);

	public boolean registerUser(String userName, String passwd, String address);

	public Player createPlayer(ResultSet rs);

	public void addWin(Player player);

	public void addLoss(Player player);

	public Player getPlayer(String playerName);
}
