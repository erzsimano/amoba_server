package hu.bme.szarch.amoba.server.service.messagehandlers;

import static hu.bme.szarch.amoba.common.config.Config.DATA_SENDER_ADDRESS;
import static hu.bme.szarch.amoba.common.config.Config.ID_LIST_GAMES;

import hu.bme.szarch.amoba.common.network.NetworkMessage;
import hu.bme.szarch.amoba.server.service.managers.base.GameManager;
import hu.bme.szarch.amoba.server.service.messagehandlers.base.GameMessageHandler;

public class ListGamesMessageHandler extends GameMessageHandler {

	public ListGamesMessageHandler(GameManager gameManager) {
		super(gameManager);
	}

	@Override
	public boolean handleMessage(NetworkMessage message) {
		if (!message.getId().equals(getHandleId()))
			return false;

		String address = message.getData(DATA_SENDER_ADDRESS);

		gameManager.listHostedGamesTo(address);

		return true;
	}

	@Override
	public String getHandleId() {
		return ID_LIST_GAMES;
	}

}
