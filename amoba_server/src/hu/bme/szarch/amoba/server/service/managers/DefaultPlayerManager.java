package hu.bme.szarch.amoba.server.service.managers;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import hu.bme.szarch.amoba.common.player.Player;
import hu.bme.szarch.amoba.server.service.managers.base.AbstractPlayerManager;
import hu.bme.szarch.amoba.server.service.managers.base.DatabaseManager;

public class DefaultPlayerManager extends AbstractPlayerManager {

	Map<String, Player> loggedOnPlayers;

	public DefaultPlayerManager(DatabaseManager dbManager) {
		super(dbManager);
	}

	@Override
	public boolean registerUser(String userName, String passwd, String address) {

		try {
			Player player = dbManager.getPlayer(userName);

			if (player == null) {
				dbManager.registerPlayer(userName, passwd);
				player = dbManager.getPlayer(userName);
			}

			player.setAddress(address);
			loggedOnPlayers.put(player.getName(), player);
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}

	@Override
	public boolean logoutUser(String userName) {
		loggedOnPlayers.remove(userName);
		return false;
	}

	@Override
	public void addWin(Player player) {
		try {
			dbManager.addPlayerWin(player.getName());
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void addLoss(Player player) {
		try {
			dbManager.addPlayerLoss(player.getName());
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public Player getPlayer(String playerName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Player createPlayer(ResultSet rs) {
		// TODO Auto-generated method stub
		return null;
	}

}
