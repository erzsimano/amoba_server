package hu.bme.szarch.amoba.server.service.messagehandlers;

import java.util.HashMap;
import java.util.Map;

import hu.bme.szarch.amoba.common.network.MessageHandler;
import hu.bme.szarch.amoba.common.network.NetworkMessageRouter;
import hu.bme.szarch.amoba.common.network.NetworkMessage;

public class ServerMessageRouter implements NetworkMessageRouter {

	private Map<String, MessageHandler> map = new HashMap<>();

	@Override
	public void routeMessage(NetworkMessage message) {
		map.get(message.getId()).handleMessage(message);
	}

	@Override
	public void registerMessageHandler(MessageHandler handler) {
		map.put(handler.getHandleId(), handler);
	}

}
