package hu.bme.szarch.amoba.server.service.messagehandlers.base;

import hu.bme.szarch.amoba.common.network.MessageHandler;
import hu.bme.szarch.amoba.server.service.managers.base.GameManager;

public abstract class GameMessageHandler implements MessageHandler {

	protected GameManager gameManager;

	protected GameMessageHandler(GameManager gameManager) {
		this.gameManager = gameManager;
	}
}
