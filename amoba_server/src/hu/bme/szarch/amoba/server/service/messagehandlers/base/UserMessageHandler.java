package hu.bme.szarch.amoba.server.service.messagehandlers.base;

import hu.bme.szarch.amoba.common.network.MessageHandler;
import hu.bme.szarch.amoba.server.service.managers.base.PlayerManager;

public abstract class UserMessageHandler implements MessageHandler {
	protected PlayerManager playerManager;

	protected UserMessageHandler(PlayerManager userManager) {
		this.playerManager = userManager;
	}
}
