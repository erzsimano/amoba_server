package hu.bme.szarch.amoba.server.service.managers;

import static hu.bme.szarch.amoba.common.config.Config.CLIENT_PORT;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import hu.bme.szarch.amoba.common.exception.InvalidMarkException;
import hu.bme.szarch.amoba.common.exception.InvalidTableFieldException;
import hu.bme.szarch.amoba.common.game.BasicGameData;
import hu.bme.szarch.amoba.common.game.GameData;
import hu.bme.szarch.amoba.common.network.NetworkMessage;
import hu.bme.szarch.amoba.common.network.MessageSenderThread;
import hu.bme.szarch.amoba.common.network.factory.NetworkObjectFactory;
import hu.bme.szarch.amoba.server.exception.GameNotFoundException;
import hu.bme.szarch.amoba.server.service.GameService;
import hu.bme.szarch.amoba.server.service.managers.base.AbstractGameManager;
import hu.bme.szarch.amoba.server.service.managers.base.DatabaseManager;
import hu.bme.szarch.amoba.server.service.managers.base.PlayerManager;

public class DefaultGameManager extends AbstractGameManager {

	protected NetworkObjectFactory messageFactory;

	public DefaultGameManager(DatabaseManager dbManager, PlayerManager userManager) {
		super(dbManager, userManager);
		messageFactory = NetworkObjectFactory.create();
	}

	@Override
	public boolean createGame(String creatorUserName, int width, int height, int winCriteria) {
		try {
			int gameId = dbManager.createGame(creatorUserName, width, height, winCriteria);
			GameData gameData = new BasicGameData(gameId, width, height, winCriteria,
					playerManager.getPlayer(creatorUserName), null);
			hostedGames.put(gameId, gameData);
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public boolean joinGame(String joinUserName, int gameId) {
		try {
			GameData gameData = hostedGames.get(gameId);

			if (gameData == null)
				throw new GameNotFoundException();

			gameData.setGuest(playerManager.getPlayer(joinUserName));

		} catch (GameNotFoundException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public boolean listHostedGamesTo(String address) {

		List<GameData> games = new ArrayList<>(hostedGames.values());
		NetworkMessage message = messageFactory.createHostedGameList(games);

		new MessageSenderThread(message, address, CLIENT_PORT).start();
		return true;
	}

	@Override
	public boolean transmitMark(int gameId, int coordX, int coordY, short mark) {
		try {
			GameService game = ongoingGames.get(gameId);

			if (game == null)
				throw new GameNotFoundException();

			game.placeMark(coordX, coordY, mark);
		} catch (GameNotFoundException | InvalidMarkException | InvalidTableFieldException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public boolean destroyGame(int gameId) {
		try {
			dbManager.deleteGame(gameId);

		} catch (SQLException e) {
			// TODO: handle exception
		}
		return false;
	}

	@Override
	public GameData createGame(ResultSet rs) {
		return null;
	}

	@Override
	protected void startGameService(GameData game) {
		GameService gameService = new GameService(this, playerManager, game);
		hostedGames.remove(game);
		ongoingGames.put(gameService.getGameId(), gameService);

		gameService.startGame();
	}
}
