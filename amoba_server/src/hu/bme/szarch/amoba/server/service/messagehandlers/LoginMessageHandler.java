package hu.bme.szarch.amoba.server.service.messagehandlers;

import static hu.bme.szarch.amoba.common.config.Config.DATA_SENDER_ADDRESS;
import static hu.bme.szarch.amoba.common.config.Config.ID_CONNECTION_REQUEST;

import hu.bme.szarch.amoba.common.config.Config.LoginFields;
import hu.bme.szarch.amoba.common.network.NetworkMessage;
import hu.bme.szarch.amoba.server.service.managers.base.PlayerManager;
import hu.bme.szarch.amoba.server.service.messagehandlers.base.UserMessageHandler;

public class LoginMessageHandler extends UserMessageHandler {

	public LoginMessageHandler(PlayerManager userManager) {
		super(userManager);
	}

	@Override
	public boolean handleMessage(NetworkMessage message) {
		if (!message.getId().equals(getHandleId()))
			return false;

		String userName = message.getData(LoginFields.DATA_USERNAME);
		String passwd = message.getData(LoginFields.DATA_PASSWORD);
		String address = message.getData(DATA_SENDER_ADDRESS);

		return playerManager.registerUser(userName, passwd, address);
	}

	@Override
	public String getHandleId() {
		return ID_CONNECTION_REQUEST;
	}

}
