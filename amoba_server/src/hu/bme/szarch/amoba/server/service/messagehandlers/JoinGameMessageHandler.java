package hu.bme.szarch.amoba.server.service.messagehandlers;

import static hu.bme.szarch.amoba.common.config.Config.ID_JOIN_GAME;

import hu.bme.szarch.amoba.common.config.Config.JoinGameFields;
import hu.bme.szarch.amoba.common.network.NetworkMessage;
import hu.bme.szarch.amoba.server.service.managers.base.GameManager;
import hu.bme.szarch.amoba.server.service.messagehandlers.base.GameMessageHandler;

public class JoinGameMessageHandler extends GameMessageHandler {

	public JoinGameMessageHandler(GameManager gameManager) {
		super(gameManager);
	}

	@Override
	public boolean handleMessage(NetworkMessage message) {
		if (!message.getId().equals(getHandleId()))
			return false;

		String joinUserName = message.getData(JoinGameFields.DATA_JOIN_USERNAME);
		int gameId = Integer.valueOf(message.getData(JoinGameFields.DATA_JOIN_GAME_ID));

		boolean result = gameManager.joinGame(joinUserName, gameId);

		if (result) {

		}

		return true;
	}

	@Override
	public String getHandleId() {
		return ID_JOIN_GAME;
	}

}
