package hu.bme.szarch.amoba.server.service.managers.base;

import java.sql.ResultSet;

import hu.bme.szarch.amoba.common.player.Player;

public abstract class AbstractPlayerManager implements PlayerManager {

	protected DatabaseManager dbManager;

	protected AbstractPlayerManager(DatabaseManager dbManager) {
		this.dbManager = dbManager;
	}

	public abstract Player createPlayer(ResultSet rs);

}
