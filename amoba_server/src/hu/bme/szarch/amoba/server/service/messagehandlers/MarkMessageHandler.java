package hu.bme.szarch.amoba.server.service.messagehandlers;

import static hu.bme.szarch.amoba.common.config.Config.ID_MARK_DATA;

import hu.bme.szarch.amoba.common.config.Config.MarkFields;
import hu.bme.szarch.amoba.common.network.NetworkMessage;
import hu.bme.szarch.amoba.server.service.managers.base.GameManager;
import hu.bme.szarch.amoba.server.service.messagehandlers.base.GameMessageHandler;

public class MarkMessageHandler extends GameMessageHandler {

	public MarkMessageHandler(GameManager gameManager) {
		super(gameManager);
	}

	@Override
	public boolean handleMessage(NetworkMessage message) {
		if (!message.getId().equals(getHandleId()))
			return false;

		int gameId = Integer.valueOf(message.getData(MarkFields.DATA_GAME_ID));
		int coordX = Integer.valueOf(message.getData(MarkFields.DATA_MARK_COORD_X));
		int coordY = Integer.valueOf(message.getData(MarkFields.DATA_MARK_COORD_Y));
		short mark = Short.valueOf(message.getData(MarkFields.DATA_MARK));

		gameManager.transmitMark(gameId, coordX, coordY, mark);

		return true;
	}

	@Override
	public String getHandleId() {
		return ID_MARK_DATA;
	}

}
