package hu.bme.szarch.amoba.server.service.managers.base;

import java.sql.ResultSet;

import hu.bme.szarch.amoba.common.game.GameData;

public interface GameManager {

	public boolean createGame(String creatorUserName, int width, int height, int winCriteria);

	public boolean joinGame(String joinUserName, int gameId);

	public boolean listHostedGamesTo(String clientAddress);

	public boolean transmitMark(int gameId, int coordX, int coordY, short mark);

	public boolean destroyGame(int gameId);

	public GameData createGame(ResultSet rs);
}
