package hu.bme.szarch.amoba.server.service.messagehandlers;

import static hu.bme.szarch.amoba.common.config.Config.ID_LOGOUT_REQUEST;

import hu.bme.szarch.amoba.common.config.Config.PlayerFields;
import hu.bme.szarch.amoba.common.network.NetworkMessage;
import hu.bme.szarch.amoba.server.service.managers.base.PlayerManager;
import hu.bme.szarch.amoba.server.service.messagehandlers.base.UserMessageHandler;

public class LogoutMessageHandler extends UserMessageHandler {

	public LogoutMessageHandler(PlayerManager playerManager) {
		super(playerManager);
	}

	@Override
	public boolean handleMessage(NetworkMessage message) {
		if (!message.getId().equals(getHandleId()))
			return false;

		String userName = message.getData(PlayerFields.DATA_PLAYER_NAME);

		playerManager.logoutUser(userName);

		return true;
	}

	@Override
	public String getHandleId() {
		return ID_LOGOUT_REQUEST;
	}

}
