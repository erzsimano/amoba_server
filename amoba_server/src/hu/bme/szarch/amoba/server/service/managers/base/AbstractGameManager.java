package hu.bme.szarch.amoba.server.service.managers.base;

import java.util.HashMap;
import java.util.Map;

import hu.bme.szarch.amoba.common.game.GameData;
import hu.bme.szarch.amoba.server.service.GameService;

public abstract class AbstractGameManager implements GameManager {

	protected Map<Integer, GameData> hostedGames;
	protected Map<Integer, GameService> ongoingGames;

	protected DatabaseManager dbManager;
	protected PlayerManager playerManager;

	protected AbstractGameManager(DatabaseManager dbManager, PlayerManager userManager) {
		this.dbManager = dbManager;
		this.playerManager = userManager;

		hostedGames = new HashMap<>();
		ongoingGames = new HashMap<>();
	}

	protected abstract void startGameService(GameData game);

}
