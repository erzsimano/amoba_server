package hu.bme.szarch.amoba.server.service;

import java.util.Random;

import hu.bme.szarch.amoba.common.config.Config;
import hu.bme.szarch.amoba.common.exception.InvalidMarkException;
import hu.bme.szarch.amoba.common.exception.InvalidTableFieldException;
import hu.bme.szarch.amoba.common.game.GameData;
import hu.bme.szarch.amoba.common.game.GameStateEvaluator;
import hu.bme.szarch.amoba.common.network.NetworkMessage;
import hu.bme.szarch.amoba.common.network.MessageSenderThread;
import hu.bme.szarch.amoba.common.network.factory.NetworkObjectFactory;
import hu.bme.szarch.amoba.common.player.Player;
import hu.bme.szarch.amoba.common.table.Default2DTable;
import hu.bme.szarch.amoba.common.table.GameTableModel;
import hu.bme.szarch.amoba.server.service.managers.base.GameManager;
import hu.bme.szarch.amoba.server.service.managers.base.PlayerManager;

public class GameService {

	private GameManager gameManager;
	private PlayerManager playerManager;

	private GameTableModel table;
	private int gameId;

	private Player playerO;
	private Player playerX;

	private Player currentPlayer;

	private NetworkObjectFactory messageFactory;

	private int winCriteria;

	public GameService(GameManager gameManager, PlayerManager playerManager, GameData game) {
		this.gameManager = gameManager;
		this.playerManager = playerManager;

		table = new Default2DTable(game.getWidth(), game.getHeight());
		gameId = game.getId();

		int random = new Random().nextInt();

		if (random % 2 == 0) {
			playerO = game.getHost();
			playerX = game.getGuest();
		} else {
			playerO = game.getGuest();
			playerX = game.getHost();
		}

		messageFactory = NetworkObjectFactory.create();

		this.winCriteria = game.getWinCriteria();
	}

	public void startGame() {
		NetworkMessage message = messageFactory.createStartGameMessage(playerX.getName(), Config.GAME_PLAYER_IDX_O);

		new MessageSenderThread(message, playerO.getAddress(), Config.CLIENT_PORT).start();

		message = messageFactory.createStartGameMessage(playerO.getName(), Config.GAME_PLAYER_IDX_X);

		new MessageSenderThread(message, playerX.getAddress(), Config.CLIENT_PORT).start();

		currentPlayer = playerO;
	}

	public void placeMark(int x, int y, short mark) throws InvalidMarkException, InvalidTableFieldException {
		table.setMark(x, y, mark);

		if (currentPlayer == playerO)
			currentPlayer = playerX;
		else
			currentPlayer = playerO;

		NetworkMessage message = messageFactory.createMarkDataObject(x, y, mark);
		new MessageSenderThread(message, currentPlayer.getAddress(), Config.CLIENT_PORT).start();

		int state = GameStateEvaluator.evaluateGameState(table, winCriteria, x, y, mark);

		switch (state) {
		case GameStateEvaluator.STATE_X_WON:
			playerManager.addWin(playerX);
			playerManager.addLoss(playerO);
			break;
		case GameStateEvaluator.STATE_O_WON:
			playerManager.addWin(playerO);
			playerManager.addLoss(playerX);
			break;
		}
	}

	public void endGame() {
		NetworkMessage message = messageFactory.createEndGameMessage();

		new MessageSenderThread(message, playerO.getAddress(), Config.CLIENT_PORT).start();
		new MessageSenderThread(message, playerX.getAddress(), Config.CLIENT_PORT).start();
	}

	public int getGameId() {
		return gameId;
	}
}
